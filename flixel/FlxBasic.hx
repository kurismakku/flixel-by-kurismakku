package flixel;

import classes.achievements.Quests;
import classes.JoySound;
import classes.JoySoundManager;
import explosions.Explosion;
import flash.geom.Point;
import flixel.group.FlxTypedGroup;
import flixel.util.FlxRandom;
import hud.QuickMenu;
import playerpack.Player;
import playerpack.PlayerBullet;
import flixel.FlxG;
import flixel.system.FlxCollisionType;
import flixel.util.FlxStringUtil;

/**
 * This is a useful "generic" Flixel object. Both <code>FlxObject</code> and 
 * <code>FlxGroup</code> extend this class. Has no size, position or graphical data.
 */
class FlxBasic implements IDestroyable
{
public var deathName:String = "";
	private var state:Int = 0;
	public var immortal:Bool = false;
	
	public static var _quests:Quests;
	@:isVar var quests( get_quests, set_quests ):Quests;
	
	public function get_quests():Quests
	{
	 
	   return _quests;
	 
	}
	 
	public function set_quests( quests:Quests):Quests
	{
	 
	   return _quests = quests;
	 
	}
	 
	public static var _soundManager:JoySoundManager;
	@:isVar var soundManager( get_soundManager, set_soundManager ):JoySoundManager;
	
	public function get_soundManager():JoySoundManager
	{
	 
	   return _soundManager;
	 
	}
	 
	public function set_soundManager( soundManager:JoySoundManager):JoySoundManager
	{
	 
	   return _soundManager = soundManager;
	 
	}
	
 
	
	private function _playSound( sound:Dynamic, isLooped:Bool = false, vol:Float = 1 ):JoySound
	{
		var sound:JoySound = soundManager.playSound( sound, vol, 0, isLooped );
		return sound;
	}
	
	private function _playMusic( sound:Dynamic, isLooped:Bool = false, vol:Float = 1 ):JoySound
	{
		var sound:JoySound = soundManager.playMusic( sound, vol, 0, isLooped );
		return sound;
	}
	
	private function toggleMuteMusic():Void
	{
		if ( soundManager.isMusicMuted() ) soundManager.unmuteMusic();
		else soundManager.muteMusic();
			
		stats.save( stats.musicMute, soundManager.isMusicMuted() );
	}
	private function toggleMuteSFX():Void
	{
		if ( soundManager.areSoundsMuted ) soundManager.unmuteSounds();
		else soundManager.muteSounds();
		
		stats.save( stats.sfxMute, soundManager.areSoundsMuted );
		
	}
	private function togglePause():Void
	{ 
		if ( FlxG.paused )
		{
			FlxG.game.onFocus();
		}
		else
		{
			FlxG.game.onFocusLost();
		}
	}
	
	public function log( data:Dynamic, autoLog:Bool = false ):Void
	{
		if ( FlxG.keys.pressed.Q || autoLog )
		{
			//GameConsole.log( data );
		}
	}
	
	public function logs( array:Array<Dynamic>, autoLog:Bool = false):Void
	{
		var finalLog:String = "";
		for ( i in 0...array.length )
		{
			finalLog +=   Std.string( array[i] ) + " , ";  // Std.string( Type.getClassName( Type.getClass(array[i]) ) ) + " : " +
		}
		log( finalLog, autoLog );
	}
	
	
	//public function isType( type:Class<Dynamic> ) :Bool {	 return Std.is(  Type.typeof( this ), type ); }
	
	
	private var isReducingDamageOverTime:Bool = false;
	@:isVar var damage( get_damage, set_damage ):Float;
	
	public function get_damage():Float
	{
	   	if (isReducingDamageOverTime == false)
		{
			return damage;
		}
		else
		{ 
			return damage * timeElapsed;
		}
	}
	 
	public function set_damage(value:Float):Float
	{
	 
		return damage = value;
	 
	}
	
	
	public var scrollsWithCamera:Bool = true;
	public var inheritScrollingFromGroup:Bool  = false;
	
	public static var _enemies:FlxTypedGroup<Enemy>;
	public static var _eBullets:FlxTypedGroup<Bullet>;
	public static var _pBullets:FlxTypedGroup<EnemyHitBullet>;
	public static var _effects:FlxTypedGroup<Effect>;
	public static var _explosions:FlxTypedGroup<Explosion>;
	
	public static var _roomSize:Point;
	public static var _gameSize:Point;
	public static var _gamePos:Point;
	public static var _game:Game;
	public static var _player:Player;
	public static var _level:Level;
 
	public static var _stats:Stats;
	
	public var layer:FlxTypedGroup<Dynamic>;
	public var parent:FlxSprite;
	
	
	@:isVar public var stats( get_stats, set_stats ):Stats;
	
	public function get_stats():Stats
	{
	 
	   return _stats;
	 
	}
	 
	public function set_stats(value:Stats):Stats
	{
	 
	   return _stats = value;
	 
	}
	
	@:isVar var gameSize( get_gameSize, set_gameSize ):Point;
	
	public function get_gameSize():Point
	{
	 
	   return _gameSize;
	 
	}
	 
	public function set_gameSize(value:Point):Point
	{
	 
	   return _gameSize = value;
	 
	}
	
	@:isVar var roomSize( get_roomSize, set_roomSize ):Point;
	
	public function get_roomSize():Point
	{
	 
	   return _roomSize;
	 
	}
	 
	public function set_roomSize(value:Point):Point
	{
	 
	   return _roomSize = value;
	 
	}
	
	@:isVar var gamePos( get_gamePos, set_gamePos ):Point;
	
	public function get_gamePos():Point
	{
	 
	   return _gamePos;
	 
	}
	 
	public function set_gamePos(value:Point):Point
	{
	 
	   return _gamePos = value;
	 
	}
	
	@:isVar var game( get_game, set_game ):Game;
	
	public function get_game():Game
	{
	 
	   return _game;
	 
	}
	 
	public function set_game(value:Game):Game
	{
	   if ( _game != null ) _game = null;
	   return _game = value;
	 
	}
	
	@:isVar var player( get_player, set_player ):Player;
	
	public function get_player():Player
	{
	 
	   return _player;
	 
	}
	 
	public function set_player(value:Player):Player
	{
	   if ( _player != null ) _player = null;
	   return _player = value;
	 
	}
	
	@:isVar var level( get_level, set_level ):Level;
	
	public function get_level():Level
	{
	 
	   return _level;
	 
	}
	 
	public function set_level(value:Level):Level
	{
	   if ( _level != null ) _level = null;
	   return _level = value;
	 
	}
	
 
	
	@:isVar var timeElapsed( get_timeElapsed, set_timeElapsed ):Float;
	
	public function get_timeElapsed():Float
	{
	 
		return  FlxG.elapsed;
 
	}
	 
	public function set_timeElapsed(value:Float):Float
	{
	 
	   return timeElapsed  = value * timeElapsed;
	 
	}
	
	
	@:isVar var frameElapsed( get_frameElapsed, set_frameElapsed ):Float;
	
	public function get_frameElapsed():Float
	{
	 
	   return 60 * timeElapsed;
	 
	}
	 
	public function set_frameElapsed(value:Float):Float
	{
	 
	   return frameElapsed  = value * timeElapsed;
	 
	}
	
	
	@:isVar var enemies( get_enemies, set_enemies ):FlxTypedGroup<Enemy>;
	
	public function get_enemies():FlxTypedGroup<Enemy>
	{
	 
	   return _enemies;
	 
	}
	 
	public function set_enemies(value:FlxTypedGroup<Enemy>):FlxTypedGroup<Enemy>
	{
	 
	   return _enemies = value;
	 
	}
	
	@:isVar var pBullets( get_pBullets, set_pBullets ):FlxTypedGroup<EnemyHitBullet>;
	
	public function get_pBullets():FlxTypedGroup<EnemyHitBullet>
	{
	 
	   return _pBullets;
	 
	}
	 
	public function set_pBullets(value:FlxTypedGroup<EnemyHitBullet>):FlxTypedGroup<EnemyHitBullet>
	{
	 
	   return _pBullets = value;
	 
	}
	
	@:isVar var eBullets( get_eBullets, set_eBullets ):FlxTypedGroup<Bullet>;
	
	public function get_eBullets():FlxTypedGroup<Bullet>
	{
	 
	   return _eBullets;
	 
	}
	 
	public function set_eBullets(value:FlxTypedGroup<Bullet>):FlxTypedGroup<Bullet> 
	{
	 
	   return _eBullets = value;
	 
	}
	
	
	@:isVar var effects( get_effects, set_effects ):FlxTypedGroup<Effect>;
	
	public function get_effects():FlxTypedGroup<Effect>
	{
	 
	   return _effects;
	 
	}
	 
	public function set_effects(value:FlxTypedGroup<Effect>):FlxTypedGroup<Effect>
	{
	 
	   return _effects = value;
	 
	}
	
	@:isVar var explosions( get_explosions, set_explosions ):FlxTypedGroup<Explosion>;
	
	public function get_explosions():FlxTypedGroup<Explosion>
	{
	 
	   return _explosions;
	 
	}
	 
	public function set_explosions(value:FlxTypedGroup<Explosion>):FlxTypedGroup<Explosion>
	{
	 
	   return _explosions = value;
	 
	}
	
//	
 
  
	
	 
	private function random():Float
	{
		return FlxRandom.float();
	}
	private function randomRange( a:Float, b:Float ):Float
	{
		return FlxRandom.floatRanged( a, b );
	}
	
	
	public static var DEG_TO_RAD:Float = Math.PI / 180;
	public static var RAD_TO_DEG:Float = 180 / Math.PI;
	
	private function logx(val:Float, base:Float = 10):Float
	{
		return Math.log(val) / Math.log(base);
	}
	
	private function scaleclamp(Arg0:Float, Arg1:Float, Arg2:Float, Arg3:Float, Arg4:Float):Float
	{
		  
		if (Arg4 > Arg3)
		{
			
			return Math.max(  Math.min( Arg3 + ((Arg0 - Arg1) / (Arg2 - Arg1)) * (Arg4 - Arg3), Arg4 ), Arg3 );
		}
		else
		{
			return Math.max( Math.min( Arg3 + ((Arg0 - Arg1) / (Arg2 - Arg1)) * (Arg4 - Arg3), Arg3 ), Arg4 );
		}
		
	}
	
	private function lengthdir_x(H:Float, A:Float):Float
	{
		return Math.cos(A * DEG_TO_RAD ) * H;
	}
	
	private function lengthdir_y(H:Float, A:Float):Float
	{
		return Math.sin(A * DEG_TO_RAD ) * H;
	}
	
	private function choose(Options:Array<Dynamic>):Dynamic
	{
		var rnd:Int = Math.floor( Math.random() * Options.length );
		
		return Options[rnd];
	}
		
	private function sign(Num:Float):Float
	{
		if (Num >= 0)
		{
			
			return 1;
		}
		else
		{
			
			return -1;
		}
	}
	/**
	 * IDs seem like they could be pretty useful, huh?
	 * They're not actually used for anything yet though.
	 */
	public var ID:Int = -1;
	/**
	 * An array of camera objects that this object will use during <code>draw()</code>. This value will initialize itself during the first draw to automatically
	 * point at the main camera list out in <code>FlxG</code> unless you already set it. You can also change it afterward too, very flexible!
	 */
	public var cameras:Array<FlxCamera>;
	/**
	 * Controls whether <code>update()</code> is automatically called by FlxState/FlxGroup.
	 */
	public var active(default, set):Bool = true;
	/**
	 * Controls whether <code>draw()</code> is automatically called by FlxState/FlxGroup.
	 */
	public var visible(default, set):Bool = true;
	/**
	 * Useful state for many game objects - "dead" (!alive) vs alive.
	 * <code>kill()</code> and <code>revive()</code> both flip this switch (along with exists, but you can override that).
	 */
	public var alive(default, set):Bool = true;
	/**
	 * This flag indicates whether this objects has been destroyed or not. Cannot be set, use <code>destroy()</code> and <code>revive()</code>.
	 */
	public var exists(default, set):Bool = true;
	
	/**
	 * Enum that informs the collision system which type of object this is (to avoid expensive type casting).
	 */
	public var collisionType(default, null):FlxCollisionType;
	
	#if !FLX_NO_DEBUG
	/**
	 * Setting this to true will prevent the object from appearing
	 * when the visual debug mode in the debugger overlay is toggled on.
	 */
	public var ignoreDrawDebug:Bool = false;
	/**
	 * Static counters for performance tracking.
	 */
	static public var _ACTIVECOUNT:Int = 0;
	static public var _VISIBLECOUNT:Int = 0;
	#end
	
	public function new() 
	{ 
		collisionType = FlxCollisionType.NONE;
	}
	
	/**
	 * WARNING: This will remove this object entirely. Use <code>kill()</code> if you want to disable it temporarily only and <code>revive()</code> it later.
	 * Override this function to null out variables manually or call destroy() on class members if necessary. Don't forget to call super.destroy()!
	 */
	public function destroy():Void 
	{
		exists = false;
		collisionType = null;
	}
	
	/**
	 * Handy function for "killing" game objects. Use <code>reset()</code> to revive them. Default behavior is to flag them as nonexistent AND dead. However, if you want the 
	 * "corpse" to remain in the game, like to animate an effect or whatever, you should override this, setting only alive to false, and leaving exists true.
	 */
	public function kill():Void
	{
		alive = false;
		exists = false;
	}
	
	/**
	 * Handy function for bringing game objects "back to life". Just sets alive and exists back to true.
	 * In practice, this function is most often called by <code>FlxObject.reset()</code>.
	 */
	public function revive():Void
	{
		alive = true;
		exists = true;
	}
	
	/**
	 * Override this function to update your class's position and appearance.
	 * This is where most of your game rules and behavioral code will go.
	 */
	public function update():Void 
	{ 
		#if !FLX_NO_DEBUG
		_ACTIVECOUNT++;
		#end
	}
	
	/**
	 * Override this function to control how the object is drawn.
	 * Overriding <code>draw()</code> is rarely necessary, but can be very useful.
	 */
	public function draw():Void
	{
		if (cameras == null)
		{
			cameras = FlxG.cameras.list;
		}
		var camera:FlxCamera;
		var i:Int = 0;
		var l:Int = cameras.length;
		while(i < l)
		{
			camera = cameras[i++];
			
			#if !FLX_NO_DEBUG
			_VISIBLECOUNT++;
			#end
		}
	}
	
	#if !FLX_NO_DEBUG
	public function drawDebug():Void
	{
		if (!ignoreDrawDebug)
		{
			var i:Int = 0;
			if (cameras == null)
			{
				cameras = FlxG.cameras.list;
			}
			var l:Int = cameras.length;
			while (i < l)
			{
				drawDebugOnCamera(cameras[i++]);
			}
		}
	}
	
	/**
	 * Override this function to draw custom "debug mode" graphics to the
	 * specified camera while the debugger's visual mode is toggled on.
	 * @param	Camera	Which camera to draw the debug visuals to.
	 */
	public function drawDebugOnCamera(?Camera:FlxCamera):Void { }
	#end
	
	/**
	 * Property setters, to provide override functionality in sub-classes
	 */
	private function set_visible(Value:Bool):Bool
	{
		return visible = Value;
	}
	private function set_active(Value:Bool):Bool
	{
		return active = Value;
	}
	private function set_alive(Value:Bool):Bool
	{
		return alive = Value;
	}
	private function set_exists(Value:Bool):Bool
	{
		return exists = Value;
	}
	
	/**
	 * Convert object to readable string name.  Useful for debugging, save games, etc.
	 */
	public function toString():String
	{
		return FlxStringUtil.getClassName(this, true);
	}
}