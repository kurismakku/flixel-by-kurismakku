package flixel;
import flash.display.BitmapData;
import flixel.group.FlxGroup;
import flixel.group.FlxTypedGroup;
import flixel.util.FlxPoint;
import flash.geom.Point;
import flash.geom.Rectangle;
import objects.Part;

/**
 * ...
 * @author 
 */
class PH
{

	    public static var roomSpeed:Float= 1;
		
		public static var DEG_TO_RAD:Float = Math.PI / 180;
		public static var RAD_TO_DEG:Float = 180 / Math.PI;
		
		public static function isInArray( element:Dynamic, array:Array<Dynamic> ):Bool
		{
			
			for ( i in 0...array.length  )
			{
				if ( element ==  array[i]  ) return true;
			}
			return false;
		}
	
		
		
	public static function splitBitmap( sourceSprite:FlxSprite, _columns:Int, _rows:Int, ObjectClass:Class<FlxBasic> = null, ContructorArgs:Array<Dynamic> = null, group:FlxTypedGroup<Dynamic> = null, recycle:Bool = true   ):Array<Dynamic>
	{
		//return [];
		  	
			if (ContructorArgs == null)
			{
				ContructorArgs = [];
			}
			var pieces:Array<Dynamic> = [];
			var _source:BitmapData = sourceSprite.framePixels;
			var _bitmapWidth:Int = _source.width;
			var _bitmapHeight:Int = _source.height;
			
			var _onePieceWidth:Int = Math.floor(_bitmapWidth / _columns);
			var _onePieceHeight:Int = Math.floor(_bitmapHeight / _rows);
		 
			var firstDistX:Float = -1;
			var firstDistY:Float = -1;
			
			var _copyRect:Rectangle = new Rectangle(0, 0, _onePieceWidth, _onePieceHeight);
			for( i in 0..._columns )
			{
	 
				for(  j in 0..._rows )
				{ 
				 
					_copyRect.x = i * _onePieceWidth;
					_copyRect.y = j * _onePieceHeight;
			 
					
					
					var tempSprite:FlxSprite;
					if ( group == null || recycle == false )
					{
						tempSprite = cast  Type.createInstance(ObjectClass, ContructorArgs);
						tempSprite.reset();
						
					}
					else
					{
						tempSprite =  cast group.recycle( ObjectClass, ContructorArgs )  ;
						tempSprite.reset();
						tempSprite.exists = true;
						tempSprite.alive = true;
					}
					
					tempSprite.exists = true;
					tempSprite.alive = true;
						
					tempSprite.pixels = new BitmapData(  _onePieceWidth, _onePieceHeight, true, 0xFF0000CC);
					tempSprite.pixels.copyPixels( _source, _copyRect, new Point() );
					tempSprite.framePixels = tempSprite.pixels;
					
					//tempSprite.attachPositionTo( new Point( sourceSprite.x, sourceSprite.y ), 0, new Point( i * (_onePieceWidth) , j * (_onePieceHeight) ), sourceSprite );
					var pos:Point = PH.attachToPart( tempSprite, sourceSprite, new Point( i * ( ( _bitmapWidth / _columns ) * sourceSprite.scale.x) , j * ( (_bitmapHeight / _rows) * sourceSprite.scale.y) ), 0 );
					tempSprite.x = pos.x;
					tempSprite.y = pos.y;
					
					if ( firstDistX == -1 ) firstDistX =  ( sourceSprite.width - sourceSprite.width * sourceSprite.scale.x ) / 4 ;
					if ( firstDistY == -1 ) firstDistY =  ( sourceSprite.height - sourceSprite.height * sourceSprite.scale.y ) / 4 ;
					
					tempSprite.x += firstDistX;
					tempSprite.y += firstDistY;
					
					tempSprite.angle = sourceSprite.angle;
					
					tempSprite.scale.x = sourceSprite.scale.x;
					tempSprite.scale.y = sourceSprite.scale.y;
 
			 	 	 var pos:Point = PH.rotatePoint( tempSprite._x, tempSprite._y, sourceSprite._x, sourceSprite._y, tempSprite.angle );
					tempSprite._x = pos.x;
					tempSprite._y = pos.y; 
					
		 
					pieces.push( tempSprite );
		 
				}
				
			
			}
			 
			return pieces;
		}
		
		public static function quadraticBezierPoint(u:Float, anchor1:Point, anchor2:Point, control:Point):Point
		{
			var uc:Float = 1 - u;
			var posx:Float = Math.pow(uc, 2) * anchor1.x + 2 * uc * u * control.x + Math.pow(u, 2) * anchor2.x;
			var posy:Float = Math.pow(uc, 2) * anchor1.y + 2 * uc * u * control.y + Math.pow(u, 2) * anchor2.y;
			return new Point(posx, posy);
		}


		public static function quadraticBezierAngle(u:Float, anchor1:Point, anchor2:Point, control:Point):Float 
		{
			var uc:Float = 1 - u;
			var dx:Float = (uc * control.x + u * anchor2.x) - (uc * anchor1.x + u * control.x);
			var dy:Float = (uc * control.y + u * anchor2.y) - (uc * anchor1.y + u * control.y);
			return PH.RAD_TO_DEG * Math.atan2(dy, dx);
		}
		
		inline public static function logx(val:Float, base:Float = 10):Float
		{
			return Math.log(val) / Math.log(base);
		}
		
		static public  function roundDec(numIn:Float, decimalPlaces:Float):Float 
		{
			var nExp:Int =  Std.int(Math.pow(10,decimalPlaces)) ; 
			var nRetVal:Float = Math.round(numIn * nExp) / nExp;
			return nRetVal;
		}
		
		inline public static function normalizeAngle( angle:Float ):Float
		{
			if ( angle > 360 )
			{
				angle %= 360;
			}
			else if ( angle < 0 )
			{
				if ( angle < -359 ) 
				{
					angle =  (-1) * ( Math.abs( angle ) % 360 ) ;
				}
				angle += 360;
			}
			return angle;
		}
	
	
		inline static public function isAngleInZone( angle:Float, angleMiddle:Float, angleRange:Float ):Bool
		{
		
			angle = normalizeAngle( angle );
			angleMiddle = normalizeAngle( angleMiddle ); 
			
/*			angle = 360 - angle;
			angleMiddle = 360 - angle;*/
			
			var limitA:Float = angleMiddle - angleRange;
			var limitB:Float = angleMiddle + angleRange;
			
			if ( limitA < 0 )
			{
				limitA += 360;
				limitB += 360;
				angle += 360;
			}
			
			 
 
			
			if ( limitA < limitB )
			{
				if ( angle >= limitA && angle <= limitB )
				{
					return true;
				}
				else 
				{
					return false;
				}
			}
			else
			{
				if ( angle >= limitB && angle <= limitA )
				{
					return true;
				}
				else 
				{
					return false;
				}
			} 
		}
		
		inline static public function getSmallerAngleDifference( angle1:Float, angle2:Float ):Float
		{
			angle1 = normalizeAngle( angle1 );
			angle2 = normalizeAngle( angle2 );
			
			var diff1:Float =  Math.abs(angle1 - angle2) ;
			var diff2:Float = 0;
			if ( angle1 <= 180 )
			{
				diff2 += angle1;
			}
			else if ( angle1 > 180 )
			{
				diff2 += ( 360 - angle1 );
			}
			if ( angle2 <= 180 )
			{
				diff2 += angle2;
			}
			else if ( angle2 > 180 )
			{
				diff2 += ( 360 - angle2 );
			}
			
			if ( diff1 <= diff2 )
			{
				return diff1;
			}
			else
			{
				return diff2;
			}
		
		}
	
	
		inline static public function  distance(X1:Float, Y1:Float, X2:Float, Y2:Float):Float
		{
			return Math.sqrt((X2 - X1) * (X2 - X1) + (Y2 - Y1) * (Y2 - Y1));
		}
		inline static public function angleTowards(targetX:Float, targetY:Float, sourceX:Float, sourceY:Float):Float
		{
			return Math.atan2((targetY - (sourceY)), (targetX- (sourceX))) * RAD_TO_DEG ;
		}
		
		
		inline static public function setVelocity(speed:Float, direction:Float):FlxPoint
		{
			var velocity:FlxPoint = new FlxPoint();
			velocity.x =  roomSpeed * speed * Math.cos(direction * DEG_TO_RAD );
			velocity.y = roomSpeed * speed * Math.sin(direction * DEG_TO_RAD );	
			return velocity;
		}
	
	
		inline public static function limitVelocity( velocity:FlxPoint, limitVel:Float ):FlxPoint
		{
			var totVel:Float = totalVelocity( velocity );
			if ( totVel > limitVel )
			{
				var a:Float = totVel / limitVel;
				a = a * a;
				velocity.x /= a;
				velocity.y /= a;
			}
			return velocity;
		}
		
	 
		inline public static function totalVelocity( velocity:FlxPoint ):Float
		{
			return Math.sqrt( Math.pow( velocity.x, 2 ) + Math.pow( velocity.y, 2) );
		}
		
		inline static public function getDirection( velocity:FlxPoint ):Float 
		{
			return  Math.atan2(velocity.y, velocity.x) * RAD_TO_DEG;		
		}
		inline static public function setDirection(velocity:FlxPoint, direction:Float):FlxPoint
		{
			return setVelocity( totalVelocity( velocity ), direction);
		}
		
		
		inline public static function motion_add( curVelocity:FlxPoint, addSpeed:Float, addDir:Float ):FlxPoint
		{
 
			
			var addedVelocity:FlxPoint = new FlxPoint();
			addedVelocity.x = addSpeed * Math.cos( DEG_TO_RAD * addDir );
			addedVelocity.y = addSpeed * Math.sin( DEG_TO_RAD * addDir );
			
			var resultVel:FlxPoint = new FlxPoint();
			resultVel.x = curVelocity.x + addedVelocity.x;
			resultVel.y = curVelocity.y + addedVelocity.y ;
			
			return resultVel;
			
		 
		}
	
	
		
		inline static public function scaleclamp(Arg0:Float, Arg1:Float, Arg2:Float, Arg3:Float, Arg4:Float):Float
		{
			  
			if (Arg4 > Arg3)
			{
				
				return Math.max(  Math.min( Arg3 + ((Arg0 - Arg1) / (Arg2 - Arg1)) * (Arg4 - Arg3), Arg4 ), Arg3 );
			}
			else
			{
				return Math.max( Math.min( Arg3 + ((Arg0 - Arg1) / (Arg2 - Arg1)) * (Arg4 - Arg3), Arg3 ), Arg4 );
			}
			
		}
		
		
		inline public static function attachToPart( follower:Dynamic, parent:Dynamic, partOff:Point, angle:Float = 0 ):Point
		{
			
			//Fix Position
			follower.x = parent.x + partOff.x;
			follower.y = parent.y + partOff.y;
			
			//Rotate around pivot points of individual elements
			var pivotRoot:Point = new Point(0, 0);
			pivotRoot.x = parent.x + parent.origin.x;
			pivotRoot.y = parent.y + parent.origin.y;
			
			var pivotE:Point = new Point();
			pivotE.x = follower.x + follower.origin.x;
			pivotE.y = follower.y + follower.origin.y;
			
			//Fix Rotation
			var rotated:Point = rotatePoint(pivotE.x, pivotE.y, pivotRoot.x, pivotRoot.y, angle);
			follower.x = rotated.x - follower.origin.x;
			follower.y = rotated.y - follower.origin.y;
			
			return new Point(follower.x, follower.y);

		}
		
	   inline public static function rotatePoint(X:Float, Y:Float, PivotX:Float, PivotY:Float, Angle:Float,P:Point=null):Point
	   {
			   if(P == null) P = new Point();
			   var radians:Float = -Angle / 180 * Math.PI;
			   var dx:Float = X-PivotX;
			   var dy:Float = PivotY-Y;
			   P.x = PivotX + Math.cos(radians)*dx - Math.sin(radians)*dy;
			   P.y = PivotY - (Math.sin(radians)*dx + Math.cos(radians)*dy);
			   return P;
	   }
	   
	   
		   /**
		 * Helper function to determine whether there is an intersection between the two polygons described
		 * by the lists of vertices. Uses the Separating Axis Theorem
		 *
		 * @param a an array of connected points [{x:, y:}, {x:, y:},...] that form a closed polygon
		 * @param b an array of connected points [{x:, y:}, {x:, y:},...] that form a closed polygon
		 * @return true if there is any intersection between the 2 polygons, false otherwise
		 */
	   public static function checkOverlapBetweenRotatedPolygons( a:Array<Point>, b:Array<Point> ):Bool
		{
			var polygons:Array<Dynamic> = [a, b];
			var minA:Float;
			var maxA:Float;
			var projected:Float;
			var i:Int;
			var i1:Int;
			var j:Int;
			var minB:Float;
			var maxB:Float;

			for ( i in 0...polygons.length ) 
			{

				// for each polygon, look at each edge of the polygon, and determine if it separates
				// the two shapes
				var polygon:Array<Dynamic> = polygons[i];
				for ( i1 in 0...polygon.length ) 
				{

					// grab 2 vertices to create an edge
					var i2:Int = (i1 + 1) % polygon.length;
					var p1:Point = polygon[i1];
					var p2:Point = polygon[i2];

					// find the line perpendicular to this edge
					var normal:Point = new Point( p2.y - p1.y,  p1.x - p2.x );

					minA = maxA = -1;
					// for each vertex in the first shape, project it onto the line perpendicular to the edge
					// and keep track of the min and max of these values
					for ( j in 0...a.length ) 
					{
						projected = normal.x * a[j].x + normal.y * a[j].y;
						if ( minA == - 1  || projected < minA) 
						{
							minA = projected;
						}
						if ( maxA == - 1 || projected > maxA) 
						{
							maxA = projected;
						}
					}

					// for each vertex in the second shape, project it onto the line perpendicular to the edge
					// and keep track of the min and max of these values
					minB = maxB = -1;
					for ( j in 0...b.length ) 
					{
						projected = normal.x * b[j].x + normal.y * b[j].y;
						if ( minB == -1 || projected < minB) {
							minB = projected;
						}
						if ( maxB == -1 || projected > maxB) 
						{
							maxB = projected;
						}
					}

					// if there is no overlap between the projects, the edge we are looking at separates the two
					// polygons, and we know there is no overlap
					if (maxA < minB || maxB < minA) 
					{
						
						return false;
					}
				}
			}
			return true;
		}
 
	
		inline static public function lengthdir_x(H:Float, A:Float):Float
		{
			return Math.cos(A * DEG_TO_RAD ) * H;
		}
		
		inline static public function lengthdir_y(H:Float, A:Float):Float
		{
			return Math.sin(A * DEG_TO_RAD ) * H;
		}
		
		static public function random(Range:Float):Float
		{
			return Math.random() * Range;
		}
		
		
		inline static public function choose(Options:Array<Dynamic>):Dynamic
		{
			var rnd:Int = Math.floor( Math.random() * Options.length );
			
			return Options[rnd];
		}
		
		static public function chooseAndRemove(Options:Array<Dynamic>):Dynamic
		{
			var rnd:Int = Math.floor( Math.random() * Options.length );
			var res:Dynamic = Options[rnd];
			Options.splice(rnd, 1);
			return res;
		}
		
 
		inline static public function sign(Num:Float):Float
		{
			if (Num >= 0)
			{
				
				return 1;
			}
			else
			{
				
				return -1;
			}
		}
		
	 
	
/*		
		static public function lerp(T:Float, EaseType:String):Float
		{
			var
				t:Float = T,
				b:Float = 0,
				c:Float = 1.0,
				d:Float = 1.0,
				p:Float = 0.3,
				s:Float = 0,
				a:Float = 0,
				td:Float = t / d;
			
			switch(EaseType)
			{
				case "InQuad":
					return c * td * td + b;
				case "OutQuad":
					return -c * td * (t - 2) + b;
			}
			
			return T;
		}*/
	
	
	
	
	public function new() 
	{
		
	}
	
}



class ExplodePiece extends FlxSprite
{
	public var group:FlxGroup;
	private var explodeTimesCounter:Int = 0;
	public var explodeTimes:Int = 0;
	public var explodeEachSeconds:Float = 1;
	private var lifeTimer:Float = 0;
	public var columns:Int = 2;
	public var rows:Int = 2;
	
	public function new( group:FlxGroup = null, explodeTimes:Int = 0, explodeEachSeconds:Float = 1 ) 
	{
		super();
		this.group = group;
		this.explodeTimes = explodeTimes;
		this.explodeEachSeconds = explodeEachSeconds;
	}
	
	public function setExplodeChain( group:FlxGroup = null, explodeTimes:Int = 0, explodeEachSeconds:Float = 1 ):Void
	{
		this.group = group;
		this.explodeTimes = explodeTimes;
		this.explodeEachSeconds = explodeEachSeconds;
	}
	
	override public function reset():Void
	{
		super.reset();
		revive();
		exists = true;
		lifeTimer = 0;
		explodeTimesCounter = explodeTimes;
		scale.x = scale.y = 1;
	}
	override public function update():Void
	{
		super.update(); 
		scale.x -= 2 * timeElapsed;
		scale.y = scale.x;
		if ( scale.x < 0 )
		{
			kill();
			exists = false;
		}
		explodeCountdown();
	}
	
	private function explodeCountdown():Void
	{
		if ( explodeTimesCounter > 0 )
		{
			lifeTimer += timeElapsed;
			if ( lifeTimer >= explodeEachSeconds )
			{
				lifeTimer -= explodeEachSeconds;
				explodeTimesCounter--;
				explode();
			}
		}
		
	}
	
	private function explode():Void
	{
		var parts:Array<Dynamic> = PH.splitBitmap( this, 2, 2, ExplodePiece, [], group  );
		for ( i in 0...parts.length )
		{
			var part:ExplodePiece = cast parts[i];
			var lastX:Float = part._x;
			var lastY:Float = part._y;
			part.reset();
			part._x = lastX;
			part._y = lastY;
			part.setVelocity( 80 + Math.random() * 80 , Math.random() * 360 );
			part.motion_add( 3 * totalVelocity() / 4, Math.random() * 360 );
			part.angularVelocity = part.totalVelocity() ;
			part.setExplodeChain( group, explodeTimes, explodeEachSeconds );
		}
		exists = false;
		
	}
	
}