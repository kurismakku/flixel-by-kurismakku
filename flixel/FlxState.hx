package flixel;

import flixel.group.FlxGroup;
import hud.Cursor;
import hud.QuickMenu;
import objects.TransitionState;

/**
 * This is the basic game "state" object - e.g. in a simple game you might have a menu state and a animation.play state.
 * It is for all intents and purpose a fancy FlxGroup. And really, it's not even that fancy.
 */
class FlxState extends FlxGroup
{
	/**
	* Determines whether or not this state is updated even when it is not the active state. For example, if you have your game state first, and then you push a menu state on top of it,
	* if this is set to true, the game state would continue to update in the background. By default this is false, so background states will be "paused" when they are not active.
	* @default false
	*/
	public var haveTransition:Bool = true;
	public var transitionState:TransitionState;
	public var addCursor:Bool = true;
	public var cursor:Cursor;
	
	private var quickMenu:QuickMenu;
	public function addQuickMenu():Void
	{
		quickMenu = new QuickMenu();
		
	}
	
	private var bkg:FlxSprite;
	private static var levelBKGs:Array<String> = [ Data.ImgLevelBackground1, Data.ImgLevelBackground2, Data.ImgLevelBackground3, Data.ImgLevelBackground4, Data.ImgLevelBackground5, Data.ImgLevelBackground6, Data.ImgLevelBackground7, Data.ImgLevelBackground8, Data.ImgLevelBackground9, Data.ImgLevelBackground11, Data.ImgLevelBackground12, Data.ImgLevelBackground13, Data.ImgLevelBackground14, Data.ImgLevelBackground15 ];
	private function addLevelBackground():Void
	{
		 bkg  = new FlxSprite();
		bkg.loadGraphic( levelBKGs[ stats.read( stats.levelsUnlocked ) ] );
		bkg.origin.x = bkg.origin.y = 0;
		bkg.scale.x = FlxG.width / bkg.width;
		bkg.scale.y = FlxG.height / bkg.height;
		add( bkg );
	}
	public var persistentUpdate:Bool;

	/**
	* Determines whether or not this state is updated even when it is not the active state. For example, if you have your game state first, and then you push a menu state on top of it, if this is set to true, the game state would continue to be drawn behind the pause state.
	* By default this is true, so background states will continue to be drawn behind the current state. If background states are not visible when you have a different state on top, you should set this to false for improved performance.
	* @default true
	*/
	public var persistentDraw:Bool;

	private var _subState:FlxSubState;
	/**
	 * Current substate.
	 * Substates also can have substates
	 */
	public var subState(get, null):FlxSubState;

	inline private function get_subState():FlxSubState
	{
		return _subState;
	}

	/**
	 * Background color of this state
	 */
	private var _bgColor:Int;
	
	public var bgColor(get, set):Int;

	private function get_bgColor():Int
	{
		return FlxG.cameras.bgColor;
	}

	private function set_bgColor(Value:Int):Int
	{
		return FlxG.cameras.bgColor = Value;
	}

	private var _useMouse:Bool = false;

	/**
	 * Whether to show mouse pointer or not
	 */
	public var useMouse(get, set):Bool;
	
	inline private function get_useMouse():Bool { return _useMouse; }
	
	inline private function set_useMouse(Value:Bool):Bool
	{
		_useMouse = Value;
		updateMouseVisibility();
		return Value;
	}
	private function updateMouseVisibility():Void
	{
		#if !FLX_NO_MOUSE
			#if mobile
				FlxG.mouse.hide();
			#else
				if (_useMouse) { FlxG.mouse.show(); }
				else { FlxG.mouse.hide(); }
			#end
		#end
	}

	/**
	 * State constructor
	 */
	public function new()
	{
		super();

		persistentUpdate = false;
		persistentDraw = true;
		#if !FLX_NO_MOUSE
			useMouse = FlxG.mouse.visible;
		#end
	}

	/**
	 * This function is called after the game engine successfully switches states. Override this function, NOT the constructor, to initialize or set up your game state.
	 * We do NOT recommend overriding the constructor, unless you want some crazy unpredictable things to happen!
	 */
	public function create():Void 
	{
		if (  haveTransition && Std.is( this, FlxSubState ) == false  )
		{
			transitionState = new TransitionState( null, true );
		} 
	}

	override public function draw():Void
	{
		if (persistentDraw || _subState == null)
		{
			super.draw();
			if (  quickMenu != null )  quickMenu.draw();
			
		}
		
		if (_subState != null)
		{
			_subState.draw();
		}
		
	}

#if !FLX_NO_DEBUG
	override public function drawDebug():Void
	{
		if (persistentDraw || _subState == null)
		{
			super.drawDebug();
		}
		
		if (_subState != null)
		{
			_subState.drawDebug();
		}
	}
#end

	public function tryUpdate():Void
	{
		if (persistentUpdate || _subState == null)

		{ 
			update();
			 if (  quickMenu != null ) quickMenu.update();
			
		}
		
		if (_subState != null)
		{
			_subState.tryUpdate();
		}
		
		
		
		if ( soundManager != null )
		{
			 
			soundManager.clean();
		}
		
		if ( addCursor && this.cursor == null )
		{
			cursor = new Cursor();
			add( cursor );
		}
	}

	/**
	 * Manually close the sub-state
	 */
	inline public function closeSubState(Destroy:Bool = true):Void
	{
		setSubState(null, null, Destroy);
	}

	/**
	 * Set substate for this state
	 * @param	RequestedState		The FlxSubState to add
	 * @param	CloseCallback		Close callback function, which will be called after closing requestedState
	 * @param	DestroyPrevious		Whether to destroy previuos substate (if there is one) or not
	 */
	public function setSubState(RequestedState:FlxSubState, ?CloseCallback:Void->Void, DestroyPrevious:Bool = false):Void
	{
		if (_subState == RequestedState)	return;
		
		//Destroy the old state (if there is an old state)
		if(_subState != null)
		{
			_subState.close(DestroyPrevious);
		}
		
		//Finally assign and create the new state (or set it to null)
		_subState = RequestedState;
		
		if (_subState != null)
		{
			//WARNING: What if the state has already been created?
			// I'm just copying the code from "FlxGame::switchState" which doesn't check for already craeted states. :/
			_subState._parentState = this;
			
			_subState.closeCallback = CloseCallback;
			
			//Reset the input so things like "justPressed" won't interfere
			if (!persistentUpdate)
			{
				FlxG.inputs.reset();
			}
			
			if (!_subState.initialized)
			{
				_subState.initialize();
				_subState.create();
			}
		}
	}

	/**
	 * Helper method for closing substate
	 * @param	Destroy		Whether to destroy current substate (by default) or leave it as is, so closed substate can be reused many times
	 */
	private function subStateCloseHandler(Destroy:Bool = true):Void
	{
		if (_subState.closeCallback != null)
		{
			_subState.closeCallback();
		}
		
		if (Destroy)
		{
			_subState.destroy();
		}
		_subState = null;
		
		updateMouseVisibility();
	}

	override public function destroy():Void
	{
		if (_subState != null)
		{
			closeSubState();
		}
		
		super.destroy();
	}

	/**
	 * This method is called after application losts its focus.
	 * Can be useful if you using third part libraries, such as tweening engines.
	 */
	public function onFocusLost():Void { }

	/**
	 * This method is called after application gets focus.
	 * Can be useful if you using third part libraries, such as tweening engines.
	 */
	public function onFocus():Void { }

	/**
	 * This function is called whenever the window size has been changed.
	 * @param 	Width	The new window width
	 * @param 	Height	The new window Height
	 */  
	public function onResize(Width:Int, Height:Int):Void { }
}